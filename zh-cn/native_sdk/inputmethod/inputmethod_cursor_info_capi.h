/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_INPUTMETHOD_CURSOR_INFO_CAPI_H
#define OHOS_INPUTMETHOD_CURSOR_INFO_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_cursor_info_capi.h
 *
 * @brief 提供光标信息对象的创建、销毁与读写方法。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#include "inputmethod_types_capi.h"
#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */
/**
 * @brief 光标信息。
 *
 * 光标的坐标位置、宽度和高度。
 *
 * @since 12
 */
typedef struct InputMethod_CursorInfo InputMethod_CursorInfo;

/**
 * @brief 创建一个新的{@link InputMethod_CursorInfo}实例。
 *
 * @param left 光标靠左点与物理屏幕左侧距离的绝对值。
 * @param top 光标顶点与物理屏幕上侧距离的绝对值。
 * @param width 宽度。
 * @param height 高度。
 * @return 如果创建成功，返回一个指向新创建的{@link InputMethod_CursorInfo}实例的指针。
 * 如果创建失败，对象返回NULL，可能的失败原因有应用地址空间满。
 * @since 12
 */
InputMethod_CursorInfo *OH_CursorInfo_Create(double left, double top, double width, double height);

/**
 * @brief 销毁一个{@link InputMethod_CursorInfo}实例。
 *
 * @param cursorInfo 表示指向即将被销毁的{@link InputMethod_CursorInfo}实例的指针。
 * @since 12
 */
void OH_CursorInfo_Destroy(InputMethod_CursorInfo *cursorInfo);

/**
 * @brief 设置光标信息内容。
 *
 * @param cursorInfo 表示指向{@link InputMethod_CursorInfo}实例的指针。
 * @param left 光标靠左点与物理屏幕左侧距离的绝对值。
 * @param top 光标顶点与物理屏幕上侧距离的绝对值。
 * @param width 宽度。
 * @param height 高度。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_CursorInfo_SetRect(
    InputMethod_CursorInfo *cursorInfo, double left, double top, double width, double height);

/**
 * @brief 获取光标信息内容。
 *
 * @param cursorInfo 表示指向{@link InputMethod_CursorInfo}实例的指针。
 * @param left 靠左点与物理屏幕左侧距离的绝对值。
 * @param top 顶点与物理屏幕上侧距离的绝对值。
 * @param width 宽度。
 * @param height 高度。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_CursorInfo_GetRect(
    InputMethod_CursorInfo *cursorInfo, double *left, double *top, double *width, double *height);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_CURSOR_INFO_CAPI_H