/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CRYPTO_SIGNATURE_H
#define CRYPTO_SIGNATURE_H

/**
 * @addtogroup CryptoSignatureApi
 * @{
 *
 * @brief 为应用提供验签接口。
 *
 * @since 12
 */

/**
 * @file crypto_signature.h
 *
 * @brief 定义验签接口。
 *
 * @library libohcrypto.z.so
 * @kit Crypto Architecture Kit
 * @syscap SystemCapability.Security.CryptoFramework
 * @since 12
 */

#include "crypto_common.h"
#include "crypto_asym_key.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义签名验签参数类型。
 *
 * @since 12
 */
typedef enum {
    /** 表示RSA算法中，使用PSS模式时，消息摘要功能的算法名。*/
    CRYPTO_PSS_MD_NAME_STR = 100,
    /** 表示RSA算法中，使用PSS模式时，掩码生成算法（目前仅支持MGF1）。*/
    CRYPTO_PSS_MGF_NAME_STR = 101,
    /** 表示RSA算法中，使用PSS模式时，MGF1掩码生成功能的消息摘要参数。*/
    CRYPTO_PSS_MGF1_NAME_STR = 102,
    /** 表示RSA算法中，使用PSS模式时，盐值的长度，长度以字节为单位。*/
    CRYPTO_PSS_SALT_LEN_INT = 103,
    /** 表示RSA算法中，使用PSS模式时，用于编码操作的整数，值为1。*/
    CRYPTO_PSS_TRAILER_FIELD_INT = 104,
    /** 表示SM2算法中，用户身份标识字段。*/
    CRYPTO_SM2_USER_ID_DATABLOB = 105,
} CryptoSignature_ParamType;

/**
 * @brief 定义验签结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoVerify OH_CryptoVerify;

/**
 * @brief 创建验签实例。
 *
 * @param algoName 用于生成验签实例的算法名称。例如：RSA1024|PKCS1|SHA256
 * @param verify 指向验签实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoVerify_Create(const char *algoName, OH_CryptoVerify **verify);

/**
 * @brief 传入公钥初始化验签实例。
 *
 * @param ctx 指向验签实例。
 * @param pubKey 公钥对象。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @see OH_CryptoVerify_Update
 * @see OH_CryptoVerify_Final
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoVerify_Init(OH_CryptoVerify *ctx, OH_CryptoPubKey *pubKey);

/**
 * @brief 追加待验签数据。
 *
 * @param ctx 指向验签实例。
 * @param in 传入的消息。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @see OH_CryptoVerify_Init
 * @see OH_CryptoVerify_Final
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoVerify_Update(OH_CryptoVerify *ctx, Crypto_DataBlob *in);

/**
 * @brief 对数据进行验签。
 *
 * @param ctx 指向验签实例。
 * @param in 传入的数据。
 * @param out 签名数据。
 * @return Return bool类型代表验签是否通过。
 * @see OH_CryptoVerify_Init
 * @see OH_CryptoVerify_Update
 * @since 12
 */
bool OH_CryptoVerify_Final(OH_CryptoVerify *ctx, Crypto_DataBlob *in, Crypto_DataBlob *signData);

/**
 * @brief 对签名数据进行恢复操作。
 *
 * @param ctx 指向验签实例。
 * @param signData 签名数据。
 * @param rawSignData 验签恢复的数据。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoVerify_Recover(OH_CryptoVerify *ctx, Crypto_DataBlob *signData,
    Crypto_DataBlob *rawSignData);

/**
 * @brief 获取验签算法名称。
 *
 * @param ctx 指向验签实例。
 * @return Return 返回验签算法名称。
 * @since 12
 */
const char *OH_CryptoVerify_GetAlgoName(OH_CryptoVerify *ctx);

/**
 * @brief 设置验签参数。
 *
 * @param ctx 指向验签实例。
 * @param type 用于指定需要设置的验签参数。
 * @param value 用于指定验签参数的具体值。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoVerify_SetParam(OH_CryptoVerify *ctx, CryptoSignature_ParamType type,
    Crypto_DataBlob *value);

/**
 * @brief 获取验签参数。
 *
 * @param ctx 指向验签实例。
 * @param type 用于指定需要获取的验签参数。
 * @param value 获取的验签参数的具体值。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoVerify_GetParam(OH_CryptoVerify *ctx, CryptoSignature_ParamType type,
    Crypto_DataBlob *value);

/**
 * @brief 销毁验签实例。
 *
 * @param ctx 指向验签实例。
 * @since 12
 */
void OH_CryptoVerify_Destroy(OH_CryptoVerify *ctx);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* CRYPTO_SIGNATURE_H */
