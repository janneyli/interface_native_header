/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CRYPTO_SYM_CIPHER_H
#define CRYPTO_SYM_CIPHER_H

/**
 * @addtogroup CryptoSymCipherApi
 * @{
 *
 * @brief 为应用提供对称密钥加解密相关接口功能。
 *
 * @since 12
 */

/**
 * @file crypto_sym_cipher.h
 *
 * @brief 定义对称密钥加解密接口。
 *
 * @library libohcrypto.z.so
 * @kit Crypto Architecture Kit
 * @syscap SystemCapability.Security.CryptoFramework
 * @since 12
 */

#include "crypto_common.h"
#include "crypto_sym_key.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义对称加解密参数类型。
 *
 * @since 12
 */
typedef enum {
    /** 加解密参数iv。 */
    CRYPTO_IV_DATABLOB = 100,
    /** 加解密参数aad。*/
    CRYPTO_AAD_DATABLOB = 101,
    /** 加解密参数authTag。*/
    CRYPTO_TAG_DATABLOB = 102,
} CryptoSymCipher_ParamsType;

/**
 * @brief 定义对称加解密结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoSymCipher OH_CryptoSymCipher;

/**
 * @brief 定义对称加解密参数结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoSymCipherParams OH_CryptoSymCipherParams;

/**
 * @brief 创建对称密钥加解密参数实例。
 *
 * @param params 指向对称加解密参数实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymCipherParams_Create(OH_CryptoSymCipherParams **params);

/**
 * @brief 设置对称密钥加解密参数。
 *
 * @param params 指向对称密钥加解密参数实例。
 * @param paramsType 设置对称密钥加解密参数类型。
 * @param value 设置的参数值。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymCipherParams_SetParam(OH_CryptoSymCipherParams *params,
    CryptoSymCipher_ParamsType paramsType, Crypto_DataBlob *value);

/**
 * @brief 销毁对称密钥加解密参数实例。
 *
 * @param params 指向对称密钥加解密参数实例。
 * @since 12
 */
void OH_CryptoSymCipherParams_Destroy(OH_CryptoSymCipherParams *params);

/**
 * @brief 根据给定的算法名称创建对称密钥加解密实例。
 *
 * @param algoName 用于生成对称密钥加解密实例的算法名称。例如： AES128|GCM|PKCS7.
 * @param ctx 指向对称密钥加解密实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymCipher_Create(const char *algoName, OH_CryptoSymCipher **ctx);

/**
 * @brief 初始化对称密钥加解密实例。
 *
 * @param ctx 指向对称密钥加解密实例。
 * @param mod 加解密模式。
 * @param key 对称密钥。
 * @param params 指向对称密钥参数实例。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymCipher_Init(OH_CryptoSymCipher *ctx, Crypto_CipherMode mod,
    OH_CryptoSymKey *key, OH_CryptoSymCipherParams *params);

/**
 * @brief 更新加密或者解密数据操作。
 *
 * @param ctx 指向对称密钥加解密实例。
 * @param in 加密或者解密的数据。
 * @param out 更新的结果。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymCipher_Update(OH_CryptoSymCipher *ctx, Crypto_DataBlob *in, Crypto_DataBlob *out);

/**
 * @brief 输出 加/解密（分组模式产生的）剩余数据，最后结束加密或者解密数据操作
 *
 * @param ctx 指向对称密钥加解密实例。
 * @param in 要加密或解密的数据。
 * @param out 返回剩余数据的加/解密结果。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @see OH_CryptoSymCipher_Init
 * @see OH_CryptoSymCipher_Update
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymCipher_Final(OH_CryptoSymCipher *ctx, Crypto_DataBlob *in, Crypto_DataBlob *out);

/**
 * @brief 获取对称密钥加解密实例的算法名称。
 *
 * @param ctx 指向对称密钥加解密实例。
 * @return 返回对称加解密算法名称。
 * @since 12
 */
const char *OH_CryptoSymCipher_GetAlgoName(OH_CryptoSymCipher *ctx);

/**
 * @brief 销毁对称密钥加解密实例。
 *
 * @param ctx 指向对称密钥加解密实例。
 * @since 12
 */
void OH_CryptoSymCipher_Destroy(OH_CryptoSymCipher *ctx);


#ifdef __cplusplus
}
#endif

/** @} */
#endif /* CRYPTO_SYM_CIPHER_H */
