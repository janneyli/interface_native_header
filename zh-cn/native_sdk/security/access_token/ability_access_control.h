/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityAccessControl
 * @{
 *
 * @brief 提供管理进程访问控制的系统能力。
 *
 * @since 12
 */

/**
 * @file ability_access_control.h
 *
 * @brief 声明管理进程访问控制的接口。
 *
 * @library ability_access_control.so
 * @kit AbilityKit
 * @syscap SystemCapability.Security.AccessToken
 * @since 12
 */

#ifndef ABILITY_ACCESS_CONTROL_H
#define ABILITY_ACCESS_CONTROL_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 校验应用是否被授予指定的权限。
 *
 *
 * @param permission - 需要校验的权限名称，合法的权限名取值可在应用权限列表中查询。
 * @return true  - 应用已经被授予该权限。
 *         false - 应用未被授予该权限。
 * @since 12
 */
bool OH_AT_CheckSelfPermission(const char *permission);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* ABILITY_ACCESS_CONTROL_H */
