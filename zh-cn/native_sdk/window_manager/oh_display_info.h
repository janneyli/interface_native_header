/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_NATIVE_DISPLAY_INFO_H
#define OH_NATIVE_DISPLAY_INFO_H

/**
 * @addtogroup OH_DisplayManager
 * @{
 *
 * @brief 提供屏幕管理的能力。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

/**
 * @file oh_display_info.h
 *
 * @brief 提供屏幕的公共枚举、公共定义等。
 *
 * @kit ArkUI
 * 引用文件：<window_manager/oh_display_info.h>
 * @library libnative_display_manager.so
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 屏幕顺时针的旋转角度。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_Rotation {
    /** 代表屏幕顺时针旋转角度0度。 */
    DISPLAY_MANAGER_ROTATION_0 = 0,

    /** 代表屏幕顺时针旋转角度90度。 */
    DISPLAY_MANAGER_ROTATION_90 = 1,

    /** 代表屏幕顺时针旋转角度180度。 */
    DISPLAY_MANAGER_ROTATION_180 = 2,

    /** 代表屏幕顺时针旋转角度270度。 */
    DISPLAY_MANAGER_ROTATION_270 = 3,
} NativeDisplayManager_Rotation;

/**
 * @brief 屏幕的旋转方向。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_Orientation {
    /** 表示设备当前以竖屏方式显示。 */
    DISPLAY_MANAGER_PORTRAIT = 0,

    /** 表示设备当前以横屏方式显示。 */
    DISPLAY_MANAGER_LANDSCAPE = 1,

    /** 表示设备当前以反向竖屏方式显示。 */
    DISPLAY_MANAGER_PORTRAIT_INVERTED = 2,

    /** 表示设备当前以反向横屏方式显示。 */
    DISPLAY_MANAGER_LANDSCAPE_INVERTED = 3,

    /** 表示显示未识别屏幕方向。 */
    DISPLAY_MANAGER_UNKNOWN,
} NativeDisplayManager_Orientation;

/**
 * @brief 屏幕管理接口返回状态码枚举。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_ErrorCode {
    /** 成功。 */
    DISPLAY_MANAGER_OK = 0,

    /** 权限校验失败，应用无权限使用该API，需要申请权限。 */
    DISPLAY_MANAGER_ERROR_NO_PERMISSION = 201,

    /** 权限校验失败，非系统应用使用了系统API。 */
    DISPLAY_MANAGER_ERROR_NOT_SYSTEM_APP = 202,

    /** 参数检查失败。 */
    DISPLAY_MANAGER_ERROR_INVALID_PARAM = 401,

    /** 该设备不支持此API。 */
    DISPLAY_MANAGER_ERROR_DEVICE_NOT_SUPPORTED = 801,

    /** 操作的显示设备无效。 */
    DISPLAY_MANAGER_ERROR_INVALID_SCREEN = 1400001,

    /** 当前操作对象无操作权限。 */
    DISPLAY_MANAGER_ERROR_INVALID_CALL = 1400002,

    /** 系统服务工作异常。 */
    DISPLAY_MANAGER_ERROR_SYSTEM_ABNORMAL = 1400003,
} NativeDisplayManager_ErrorCode;

/**
 * @brief 可折叠设备的显示模式枚举。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_FoldDisplayMode {
    /** 表示设备当前折叠显示模式未知。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_UNKNOWN = 0,

    /** 表示设备当前全屏显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_FULL = 1,

    /** 表示设备当前主屏幕显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_MAIN = 2,

    /** 表示设备当前子屏幕显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_SUB = 3,

    /** 表示设备当前双屏协同显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_COORDINATION = 4,
} NativeDisplayManager_FoldDisplayMode;

/**
 * @brief 矩形区域。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_Rect {
    /** 矩形区域左边界。 */
    int32_t left;

    /** 矩形区域上边界。 */
    int32_t top;

    /** 矩形区域宽度。 */
    uint32_t width;

    /** 矩形区域高度。 */
    uint32_t height;
} NativeDisplayManager_Rect;

/**
 * @brief 瀑布屏曲面部分显示区域。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_WaterfallDisplayAreaRects {
    /** 瀑布屏左侧不可用矩形区域。 */
    NativeDisplayManager_Rect left;

    /** 瀑布屏顶部不可用矩形区域。 */
    NativeDisplayManager_Rect top;

    /** 瀑布屏右侧不可用矩形区域。 */
    NativeDisplayManager_Rect right;

    /** 瀑布屏底部不可用矩形区域。 */
    NativeDisplayManager_Rect bottom;
} NativeDisplayManager_WaterfallDisplayAreaRects;

/**
 * @brief 挖孔屏、刘海屏、瀑布屏等不可用屏幕区域信息。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_CutoutInfo {
    /** 挖孔屏、刘海屏不可用屏幕区域长度。 */
    int32_t boundingRectsLength;

    /** 挖孔屏、刘海屏等区域的边界矩形。 */
    NativeDisplayManager_Rect *boundingRects;

    /** 瀑布屏曲面部分显示区域。 */
    NativeDisplayManager_WaterfallDisplayAreaRects waterfallDisplayAreaRects;
} NativeDisplayManager_CutoutInfo;

#ifdef __cplusplus
}
#endif
/** @} */
#endif // OH_NATIVE_DISPLAY_INFO_H