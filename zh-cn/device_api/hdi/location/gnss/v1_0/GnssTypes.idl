/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiGnss
 * @{
 *
 * @brief 定义GNSS模块的接口。
 *
 * 上层GNSS服务可以获取一个GNSS驱动对象或代理，然后调用该对象或代理提供的api来访问GNSS设备，
 * 从而实现启动GNSS芯片，启动导航，设置GNSS工作模式，注入参考信息，获取定位结果，获取nmea，
 * 获取卫星状态信息，批量获取缓存位置信息等。
 *
 * @since 3.2
 */

/**
 * @file GnssTypes.idl
 *
 * @brief 定义GNSS模块接口中使用到的数据结构。
 *
 * 模块包路径：ohos.hdi.location.gnss.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.gnss.v1_0;

/**
 * @brief 定义GNSS工作模式。
 *
 * 定义GNSS工作模式的枚举值。
 *
 * @since 3.2
 */
enum GnssWorkingMode
{
    /** GNSS独立模式（无辅助） */
    GNSS_WORKING_MODE_STANDALONE = 1,

    /** 在移动设备端进行定位计算的模式 */
    GNSS_WORKING_MODE_MS_BASED = 2,

    /** 移动设备辅助模式，在网络侧完成定位计算 */
    GNSS_WORKING_MODE_MS_ASSISTED = 3
};

/**
 * @brief 定义GNSS启动方式。
 *
 * 定义GNSS启动类型的枚举值，用于区分普通GNSS定位和GNSS缓存上报功能
 * （不立刻上报底层位置，仅当上层请求全部上报或者底层FIFO满后才上报位置）。
 *
 * @since 3.2
 */
enum GnssStartType {
    /** 普通的GNSS功能 */
    GNSS_START_TYPE_NORMAL = 1,
    /** GNSS缓存位置功能 */
    GNSS_START_TYPE_GNSS_CACHE = 2,
};

/**
 * @brief 定义GNSS参考信息类型。
 *
 * 参考信息包含参考时间、参考位置等。
 *
 * @since 3.2
 */
enum GnssRefInfoType {
    /** 参考时间 */
    GNSS_REF_INFO_TIME = 1,
    /** 参考位置 */
    GNSS_REF_INFO_LOCATION = 2,
    /** 参考融合位置 */
    GNSS_REF_INFO_BEST_LOCATION = 3,
};

/**
 * @brief 定义辅助数据类型。
 *
 * @since 3.2
 */
enum GnssAuxiliaryData {
    /** 星历 */
    GNSS_AUXILIARY_DATA_EPHEMERIS    = 1,
    /** 历书 */
    GNSS_AUXILIARY_DATA_ALMANAC      = 2,
    /** 位置 */
    GNSS_AUXILIARY_DATA_POSITION     = 4,
    /** 时间 */
    GNSS_AUXILIARY_DATA_TIME         = 8,
    /** 电离层 */
    GNSS_AUXILIARY_DATA_IONO         = 16,
    /** UTC时间 */
    GNSS_AUXILIARY_DATA_UTC          = 32,
    /** 健康度 */
    GNSS_AUXILIARY_DATA_HEALTH       = 64,
    /** 方向 */
    GNSS_AUXILIARY_DATA_SVDIR        = 128,
    /** 方向角 */
    GNSS_AUXILIARY_DATA_SVSTEER      = 256,
    /** 辅助数据 */
    GNSS_AUXILIARY_DATA_SADATA       = 512,
    /** 差分数据 */
    GNSS_AUXILIARY_DATA_RTI          = 1024,
    /** cell数据库 */
    GNSS_AUXILIARY_DATA_CELLDB_INFO  = 32768,
    /** 所有辅助数据 */
    GNSS_AUXILIARY_DATA_ALL          = 65535
};

/**
 * @brief 定义GNSS的工作状态。
 *
 * @since 3.2
 */
enum GnssWorkingStatus {
    /** 未知状态 */
    GNSS_STATUS_NONE           = 0,

    /** 导航启动 */
    GNSS_STATUS_SESSION_BEGIN  = 1,

    /** 导航停止 */
    GNSS_STATUS_SESSION_END    = 2,

    /** 芯片上电 */
    GNSS_STATUS_ENGINE_ON      = 3,

    /** 芯片下电 */
    GNSS_STATUS_ENGINE_OFF     = 4
};

/**
 * @brief 定义GNSS能力
 *
 * @since 3.2
 */
enum GnssCapabilities {
    /** 支持MS-Based模式 */
    GNSS_CAP_SUPPORT_MSB = 1,

    /** 支持MS-Assisted模式 */
    GNSS_CAP_SUPPORT_MSA = 2,

    /** 支持地理围栏功能 */
    GNSS_CAP_SUPPORT_GEOFENCING = 4,

    /** 支持GNSS测量信息上报 */
    GNSS_CAP_SUPPORT_MEASUREMENTS = 8,

    /** 支持GNSS导航电文上报 */
    GNSS_CAP_SUPPORT_NAV_MESSAGES = 16,

    /** 支持GNSS缓存位置功能 */
    GNSS_CAP_SUPPORT_GNSS_CACHE = 32,
};

/**
 * @brief 定义星座类型。
 *
 * @since 3.2
 */
enum GnssConstellationType {
    /** 未知 */
    GNSS_CONSTELLATION_UNKNOWN = 0,

    /** GPS */
    GNSS_CONSTELLATION_GPS     = 1,

    /** SBAS */
    GNSS_CONSTELLATION_SBAS    = 2,

    /** GLONASS */
    GNSS_CONSTELLATION_GLONASS = 3,

    /** QZSS */
    GNSS_CONSTELLATION_QZSS    = 4,

    /** 北斗 */
    GNSS_CONSTELLATION_BEIDOU  = 5,

    /** GALILEO */
    GNSS_CONSTELLATION_GALILEO = 6,

    /** IRNSS */
    GNSS_CONSTELLATION_IRNSS   = 7,
};

/**
 * @brief 定义卫星状态中的附加信息。
 *
 * @since 3.2
 */
enum SatellitesStatusFlag {
    /** 默认值 */
    SATELLITES_STATUS_NONE                  = 0,
    /** 有星历表数据 */
    SATELLITES_STATUS_HAS_EPHEMERIS_DATA    = 1,
    /** 有历书数据 */
    SATELLITES_STATUS_HAS_ALMANAC_DATA      = 2,
    /** 定位中有使用到 */
    SATELLITES_STATUS_USED_IN_FIX           = 4,
    /** 有载波频率 */
    SATELLITES_STATUS_HAS_CARRIER_FREQUENCY = 8
};

/**
 * @brief 定义卫星状态信息结构体。
 *
 * @since 3.2
 */
struct SatelliteStatusInfo {
    /** 卫星个数 */
    unsigned int satellitesNumber;

    /** 卫星编号 */
    short[] satelliteIds;

    /** 星座类型 */
    enum GnssConstellationType[] constellation;

    /** 信噪比 */
    float[] carrierToNoiseDensitys;

    /** 卫星仰角 */
    float[] elevation;

    /** 方位角 */
    float[] azimuths;

    /** 跟踪信号的载波频率 */
    float[] carrierFrequencies;

    /** 卫星状态中的附加信息 */
    enum SatellitesStatusFlag flags;
};

/**
 * @brief 定义基础的GNSS配置参数。
 *
 * @since 3.2
 */
struct GnssBasicConfig {
    /** 位置上报间隔 */
    unsigned int minInterval;

    /** 期望的定位精度 */
    unsigned int accuracy;

    /** 期望的首定位耗时 */
    unsigned int firstFixTime;

    /** 是否周期性定位 */
    boolean isPeriodicPositioning;

    /** GNSS工作模式 */
    enum GnssWorkingMode gnssMode;
};

/**
 * @brief 定义GNSS缓存功能的配置参数。
 *
 * @since 3.2
 */
struct GnssCachingConfig {
    /** 缓存位置之间的时间间隔 */
    unsigned int interval;

    /** 如果为true，在FIFO满后唤醒AP并报告缓存的位置 */
    boolean fifoFullNotify;
};

/**
 * @brief 定义GNSS配置参数结构体。
 *
 * @since 3.2
 */
struct GnssConfigPara {
    /** GNSS基础配置参数 */
    struct GnssBasicConfig gnssBasic;
    /** GNSS cache功能配置参数 */
    struct GnssCachingConfig gnssCaching;
};

/**
 * @brief 定义GNSS参考时间结构体。
 *
 * @since 3.2
 */
struct GnssRefTime {
    /** UTC时间 */
    long timeMs;
    /** 自系统启动后经过的时间 */
    long timeReferenceMs;
    /** 时间不确定度 */
    int uncertainty;
};

/**
 * @brief 定义GNSS参考位置结构体。
 *
 * @since 3.2
 */
struct GnssRefLocation {
    /** 纬度 */
    double latitude;
    /** 经度 */
    double longitude;
    /** 精确度 */
    float accuracy;
};

/**
 * @brief 定义GNSS定位结果结构体。
 *
 * @since 3.2
 */
struct LocationInfo {
    /** 纬度 */
    double latitude;
    /** 经度 */
    double longitude;
    /** 海拔 */
    double altitude;
    /** 精确度 */
    float accuracy;
    /** 速度 */
    float speed;
    /** 方向 */
    double direction;
    /** UTC时间戳 */
    long timeStamp;
    /** 自系统启动后经过的时间 */
    long timeSinceBoot;
};

/**
 * @brief 定义GNSS参考信息结构体。
 *
 * @since 3.2
 */
struct GnssRefInfo {
    /** 参考信息类型 */
    enum GnssRefInfoType type;
    /** 参考时间 */
    struct GnssRefTime time;
    /** 参考位置 */
    struct GnssRefLocation location;
    /** 参考融合位置 */
    struct LocationInfo best_location;
};
/** @} */