/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiAGnss
 * @{
 *
 * @brief 定义AGNSS模块接口。
 *
 * 上层可以使用该模块提供的接口设置AGNSS回调、AGNSS服务器地址、AGNSS参考信息、setId等。
 *
 * @since 3.2
 */

/**
 * @file IAGnssInterface.idl
 *
 * @brief 定义AGNSS接口，用于设置AGNSS回调、AGNSS服务器地址、AGNSS参考信息和setId。
 *
 * 模块包路径：ohos.hdi.location.agnss.v1_0
 *
 * 引用：
 * - ohos.hdi.location.agnss.v1_0.IAGnssCallback
 * - ohos.hdi.location.agnss.v1_0.AGnssTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.agnss.v1_0;

import ohos.hdi.location.agnss.v1_0.IAGnssCallback;
import ohos.hdi.location.agnss.v1_0.AGnssTypes;

/**
 * @brief 定义AGNSS接口，用于设置AGNSS回调、AGNSS服务器地址、AGNSS参考信息和setId。
 *
 * @since 3.2
 */
interface IAGnssInterface {
    /**
     * @brief 设置回调函数
     *
     * @param callback 表示上层传入的回调函数，包含请求上层建立或释放数据业务连接，请求上层下发setId，
     * 请求上层下发AGNSS参考信息等回调，详情参考{@link IAGnssCallback}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetAgnssCallback([in] IAGnssCallback callbackObj);

    /**
     * @brief 设置AGNSS服务器信息。
     *
     * @param server 表示AGNSS服务器信息。详情参考{@link AGnssServerInfo}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetAgnssServer([in] struct AGnssServerInfo server);

    /**
     * @brief 注入参考信息。
     *
     * @param refInfo 表示AGNSS参考信息。详情参考{@link AGnssRefInfo}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetAgnssRefInfo([in] struct AGnssRefInfo refInfo);

    /**
     * @brief 设置setId。
     *
     * @param id 表示setId，详情参考{@link SubscriberSetId}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetSubscriberSetId([in] struct SubscriberSetId id);
}
/** @} */