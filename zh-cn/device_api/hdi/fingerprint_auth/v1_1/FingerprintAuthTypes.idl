/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFingerprintAuth
 * @{
 *
 * @brief 提供指纹认证驱动的API接口。
 *
 * 指纹认证驱动程序为指纹认证服务提供统一的接口，用于访问指纹认证驱动程序。获取指纹认证驱动代理后，服务可以调用相关API获取执行器。
 * 获取指纹认证执行器后，服务可以调用相关API获取执行器信息，获取凭据模板信息、注册指纹特征模板、进行用户指纹认证、删除指纹特征模板等。
 *
 * @since 4.0
 */

/**
 * @file FingerprintAuthTypes.idl
 *
 * @brief  定义指纹认证驱动枚举和数据结构，包括认证类型、执行器角色、 执行器安全等级、命令ID、指纹提示信息编码、执行器信息和模板信息。
 *
 * 模块包路径：ohos.hdi.fingerprint_auth.v1_1
 *
 * @since 4.0
 */

package ohos.hdi.fingerprint_auth.v1_1;

/**
 * @brief 枚举提示信息编码。
 *
 * @since 4.0
 * @version 1.1
 */
enum FingerprintTipsCode : int {
    FINGERPRINT_AUTH_TIP_GOOD = 0, /**< 获取的指纹图像是完整的。 */
    FINGERPRINT_AUTH_TIP_DIRTY = 1, /**< 指纹图像非常模糊，原因是传感器上存在可疑或检测到的污垢。 */
    FINGERPRINT_AUTH_TIP_INSUFFICIENT = 2, /**< 仅检测到部分指纹图像。 */
    FINGERPRINT_AUTH_TIP_PARTIAL = 3, /**< 仅检测到部分指纹图像。 */
    FINGERPRINT_AUTH_TIP_TOO_FAST = 4, /**< 指纹图像由于快速移动而不完整。 */
    FINGERPRINT_AUTH_TIP_TOO_SLOW = 5, /**< 指纹图像由于没有移动而无法读取。 */
    FINGERPRINT_AUTH_TIP_FINGER_DOWN = 6, /**< 按下手指。 从4.0版本开始支持使用。*/
    FINGERPRINT_AUTH_TIP_FINGER_UP = 7, /**< 抬起手指。 从4.0版本开始支持使用。*/
    VENDOR_FINGERPRINT_AUTH_TIP_BEGIN = 10000 /**< 用于厂商自定义提示信息。 */
};

/**
 * @brief 获取指纹执行器属性。
 *
 * @since 4.0
 * @version 1.1
 */
enum GetPropertyType : int {
    /** 获取认证子类型。 */
    AUTH_SUB_TYPE = 1,
    /** 获取指纹剩余锁定时间。 */
    LOCKOUT_DURATION = 2,
    /** 获取指纹剩余比对次数。 */
    REMAIN_ATTEMPTS = 3,
    /** 获取指纹录入进度。 */
    ENROLL_PROGRESS = 4,
    /** 获取指纹光斑的信息。 */
    SENSOR_INFO = 5
};

/**
 * @brief 执行器属性。
 *
 * @since 4.0
 * @version 1.1
 */
struct Property {
    /** 认证子类型。 */
    unsigned long authSubType;
    /** 指纹剩余锁定时间。 */
    int lockoutDuration;
    /** 指纹剩余可重试次数。 */
    int remainAttempts;
    /** 指纹录入进度。 */
    String enrollmentProgress;
    /** 指纹光斑信息。 */
    String sensorInfo;
};

/**
 * @brief 枚举sa命令ID。
 *
 * @since 4.0
 * @version 1.1
 */
enum SaCommandId : int {
    /** 打开光斑功能。 */
    ENABLE_SENSOR_ILLUMINATION = 1,
    /** 关闭光斑功能。 */
    DISABLE_SENSOR_ILLUMINATION = 2,
    /** 点亮光斑。 */
    TURN_ON_SENSOR_ILLUMINATION = 3,
    /** 熄灭光斑。 */
    TURN_OFF_SENSOR_ILLUMINATION = 4,
};

/**
 * @brief 光斑使能的sa命令参数。
 *
 * @since 4.0
 * @version 1.1
 */
struct SaCommandParamEnableSensorIllumination {
    /** 高亮显示圆心x坐标与屏幕宽度的比值。 */
    unsigned int centerX;
    /** 高亮显示圆心y坐标与屏幕高度的比值。 */
    unsigned int centerY;
    /** 高亮显示圆半径，单位为px。 */
    unsigned int radius;
    /** 高亮显示亮度。 */
    unsigned int brightness;
    /** 高亮显示颜色。 */
    unsigned int color;
};

/**
 * @brief sa命令参数为空。
 *
 * @since 4.0
 * @version 1.1
 */
struct SaCommandParamNone {
};

/**
 * @brief sa命令参数。
 *
 * @since 4.0
 * @version 1.1
 */
union SaCommandParam {
    /** sa命令参数为空，参见{@link SaCommandParamNone}。 */
    struct SaCommandParamNone none;
    /** sa命令参数是打开光斑使能 ，参见{@link SaCommandParamEnableSensorIllumination}。 */
    struct SaCommandParamEnableSensorIllumination enableSensorIllumination;
};

/**
 * @brief sa命令ID
 *
 * @since 4.0
 * @version 1.1
 */
struct SaCommand {
    /** sa命令ID。见{@link SaCommandId}。 */
    enum SaCommandId id;
    /** sa命令参数。见{@link SaCommandParam}。 */
    union SaCommandParam param;
};

/**
 * @brief 枚举命令ID。
 *
 * @since 4.0
 * @version 1.1
 */
enum CommandId : int {
    LOCK_TEMPLATE = 1, /**< 指纹锁定的命令ID。 */
    UNLOCK_TEMPLATE = 2, /**< 指纹解锁的命令ID。 */
    INIT_ALGORITHM = 3, /**< 初始化算法的命令ID。 */
    VENDOR_COMMAND_BEGIN = 10000 /**< 用于厂商自定义提示信息。 */
};
/** @} */