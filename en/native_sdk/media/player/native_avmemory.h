/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Provides the basic backbone capabilities for the media playback framework,
 * including functions related to the memory, error code, and format carrier.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_avmemory.h
 *
 * @brief Declares the memory-related functions.
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVMEMORY_H
#define NATIVE_AVMEMORY_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_AVMemory OH_AVMemory;

/**
 * @brief Obtains the virtual memory address of an <b>OH_AVMemory</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param mem Indicates the pointer to an <b>OH_AVMemory</b> instance.
 * @return Returns the virtual address if the memory is valid.
 * @return Returns a null pointer if the memory is invalid.
 * @since 9
 * @version 1.0
 */
uint8_t *OH_AVMemory_GetAddr(struct OH_AVMemory *mem);

/**
 * @brief Obtains the memory size of an <b>OH_AVMemory</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @param mem Indicates the pointer to an <b>OH_AVMemory</b> instance.
 * @return Returns the size if the memory is valid.
 * @return Returns <b>-1</b> if the memory is invalid.
 * @since 9
 * @version 1.0
 */
int32_t OH_AVMemory_GetSize(struct OH_AVMemory *mem);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVMEMORY_H
