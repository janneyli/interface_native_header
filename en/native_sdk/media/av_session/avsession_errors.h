/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_ERRORS_H
#define OHOS_AVSESSION_ERRORS_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief Provides APIs for audio and video (AV) media control.
 *
 * The APIs can be used to create, manage, and control media sessions.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_errors.h
 *
 * @brief Declares AV session error codes.
 *
 * @since 9
 * @version 1.0
 */

#include <cinttypes>
#include "errors.h"

namespace OHOS::AVSession {
/** An error occurs. */
constexpr int32_t  AVSESSION_ERROR = -1;
/** The operation is successful. */
constexpr int32_t  AVSESSION_SUCCESS = 0;
/** Base definition of the AV session error code. */
constexpr int32_t  AVSESSION_ERROR_BASE = 1000;
/** No memory is available. */
constexpr int32_t  ERR_NO_MEMORY = -(AVSESSION_ERROR_BASE + 1);
/** The passed parameter is invalid. */
constexpr int32_t  ERR_INVALID_PARAM = -(AVSESSION_ERROR_BASE + 2);
/** The service does not exist. */
constexpr int32_t  ERR_SERVICE_NOT_EXIST = -(AVSESSION_ERROR_BASE + 3);
/** The session listener already exists. */
constexpr int32_t  ERR_SESSION_LISTENER_EXIST = -(AVSESSION_ERROR_BASE + 4);
/** An error occurs during data marshalling. */
constexpr int32_t  ERR_MARSHALLING = -(AVSESSION_ERROR_BASE + 5);
/** An error occurs during data unmarshalling. */
constexpr int32_t  ERR_UNMARSHALLING = -(AVSESSION_ERROR_BASE + 6);
/** Failed to send data through IPC. */
constexpr int32_t  ERR_IPC_SEND_REQUEST = -(AVSESSION_ERROR_BASE + 7);
/** The maximum number of sessions is reached. */
constexpr int32_t  ERR_SESSION_EXCEED_MAX = -(AVSESSION_ERROR_BASE + 8);
/** The session does not exist. */
constexpr int32_t  ERR_SESSION_NOT_EXIST = -(AVSESSION_ERROR_BASE + 9);
/** The session control command is not supported. */
constexpr int32_t  ERR_COMMAND_NOT_SUPPORT = -(AVSESSION_ERROR_BASE + 10);
/** The controller does not exist. */
constexpr int32_t  ERR_CONTROLLER_NOT_EXIST = -(AVSESSION_ERROR_BASE + 11);
/** No permission. */
constexpr int32_t  ERR_NO_PERMISSION = -(AVSESSION_ERROR_BASE + 12);
/** The session is not activated. */
constexpr int32_t  ERR_SESSION_DEACTIVE = -(AVSESSION_ERROR_BASE + 13);
/** The controller exists. */
constexpr int32_t  ERR_CONTROLLER_IS_EXIST = -(AVSESSION_ERROR_BASE + 14);
/** The ability is running. */
constexpr int32_t  ERR_START_ABILITY_IS_RUNNING = -(AVSESSION_ERROR_BASE + 15);
/** Failed to start the ability. */
constexpr int32_t  ERR_ABILITY_NOT_AVAILABLE = -(AVSESSION_ERROR_BASE + 16);
/** Ability startup timed out. */
constexpr int32_t  ERR_START_ABILITY_TIMEOUT = -(AVSESSION_ERROR_BASE + 17);
/** The maximum number of times that a control command can be sent is reached. */
constexpr int32_t ERR_COMMAND_SEND_EXCEED_MAX = -(AVSESSION_ERROR_BASE + 18);
/** Failed to send data through RPC. */
constexpr int32_t  ERR_RPC_SEND_REQUEST = -(AVSESSION_ERROR_BASE + 19);
}  // namespace OHOS::AVSession
/** @} */
#endif  // OHOS_AVSESSION_ERRORS_H
